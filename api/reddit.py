import logging
import os
import sqlite3
import time
from typing import Any

import praw
import requests
from better_profanity import profanity
from redvid import Downloader

from api import vk
from settings import reddit_settings, vk_token

profanity.load_censor_words()


class RedditBot:
    def __init__(self, subreddit: str, config: Any):
        self.subreddit_name = subreddit
        self.reddit = praw.Reddit(**reddit_settings)
        self.vk_post = vk.Post(vk_token, config.get('group_id'))
        self.group_id = config.get('group_id')
        self.over_18_allowed = config.get('over_18_allowed', False)
        self.connect = self.get_connect()
        self.subreddit = self.reddit.subreddit(self.subreddit_name)

    def post_hot(self, limit: int = 50, after: str = None, before: str = None) -> (str, bool):
        hot = self.subreddit.hot(limit=limit, params={'after': after, 'before': before})
        rpost_id = ''
        for post in hot:
            rpost_id = post.fullname
            if (post.over_18 and not self.over_18_allowed) or self._is_already_posted(post):
                continue
            if post.url.endswith(('.jpg', '.png', '.jpeg')):
                response = requests.get(post.url)
                if response.status_code != 200:
                    continue

                image = response.content
                try:
                    attachment = vk.Attachment(f"{post.fullname}.jpg", image, vk.FileType.image)
                    self.vk_post.create(f"{profanity.censor(str(post.title))}\n\nover_18: {post.over_18}",
                                        attachment,
                                        f"https://www.reddit.com{post.permalink}")
                    return rpost_id, True
                except Exception as e:
                    logging.exception(e)
                    break

            elif hasattr(post, 'media_metadata') and len(post.media_metadata) > 0:
                attachments = []
                for i, m in post.media_metadata.items():
                    if len(attachments) == 10:
                        break
                    image = m.get('p')
                    if image:
                        response = requests.get(image[-1].get('u'))
                        if response.status_code != 200:
                            continue

                        attachments.append(vk.Attachment(f"{post.fullname}.jpg", response.content, vk.FileType.image))
                        time.sleep(1)
                try:
                    self.vk_post.create(f"{profanity.censor(str(post.title))}\n\nover_18: {post.over_18}",
                                        attachments,
                                        f"https://www.reddit.com{post.permalink}")
                    return rpost_id, True
                except Exception as e:
                    logging.exception(e)
                    break

            elif post.media and post.media.get('reddit_video'):
                try:
                    reddit = Downloader(max_q=True, url=f"https://www.reddit.com{post.permalink}", log=False)
                    reddit.check()
                    if reddit.duration >= 180:
                        continue
                    path = reddit.download()
                    logging.info(f'video: {path}')
                    attachment = vk.Attachment(f"{post.title[:70].rstrip()}", path, vk.FileType.video)
                    self.vk_post.create(f"{profanity.censor(str(post.title))}\n\nover_18: {post.over_18}",
                                        attachment,
                                        f"https://www.reddit.com{post.permalink}")
                    os.remove(path)
                    return rpost_id, True
                except Exception as e:
                    logging.exception(e)
                    break
            else:
                continue
        return rpost_id, False

    def _is_already_posted(self, post) -> bool:
        with self.connect as c:
            cursor = c.cursor()
            try:
                dt = time.strftime("%Y-%m-%d %H:%M:%S")
                cursor.execute("INSERT INTO posts (post_id, title, url, post_time, subreddit)"
                               "VALUES (?, ?, ?, ?, ?);",
                               (post.fullname, post.title, post.url, dt, self.subreddit_name))
                c.commit()
            except sqlite3.IntegrityError as e:
                logging.warning(post.url)
                cursor.close()
                return True
        return False

    def avatar_update(self):
        if url := self._icon_url():
            with self.connect as c:
                cursor = c.cursor()
                try:
                    cursor.execute("INSERT INTO avatar (image_url, subreddit)"
                                   "VALUES (?, ?);", (url, self.subreddit_name))
                    c.commit()
                except sqlite3.IntegrityError:
                    cursor.close()
                    return
            try:
                if content := requests.get(url).content:
                    vk.avatar_update(vk_token, -self.group_id, content)
            except:
                return

    def _icon_url(self) -> str:
        if self.subreddit.community_icon:
            return self.subreddit.community_icon
        elif self.subreddit.icon_img:
            return self.subreddit.icon_img
        else:
            return ''

    def get_connect(self):
        db = os.path.join('db', 'reddit.sqlite')
        sql = open(os.path.join('db', 'reddit.sql')).read()
        connect = sqlite3.connect(db)
        with connect:
            cursor = connect.cursor()
            cursor.executescript(sql)
            cursor.close()
        return connect
