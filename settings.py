import os

__version__ = "0.7.0"

from dotenv import load_dotenv

load_dotenv()

subreddit_vkgroups = {
    # subreddit: {vk_group_id: 00000000, over_18_allowed: False}
    "rarepuppers": {"group_id": 192618172, "over_18_allowed": False},
    "blop": {"group_id": 192620817, "over_18_allowed": False},
    "ProgrammerHumor": {"group_id": 192629226, "over_18_allowed": False},
    "WhatsWrongWithYourDog": {"group_id": 192620835, "over_18_allowed": False},
    "aww": {"group_id": 192620778, "over_18_allowed": False},
    "hololive": {"group_id": 202891523, "over_18_allowed": False},
    "GawrGura": {"group_id": 202891698, "over_18_allowed": False},
    "sketches": {"group_id": 173917023, "over_18_allowed": False},
}

# VK
vk_token = os.getenv('VK_TOKEN')

# Reddit
reddit_settings = {
    "username": os.getenv('REDDIT_USERNAME'),
    "password": os.getenv('REDDIT_PASSWORD'),
    "client_secret": os.getenv('REDDIT_CLIENT_SECRET'),
    "client_id": os.getenv('REDDIT_CLIENT_ID'),
    "user_agent": f"python:InfiniteVkBot:v{__version__} (by /u/{os.getenv('REDDIT_USERNAME')})",
}

# logging

logger = {
    'version': 1,
    'formatters': {
        'default': {
            'format': '%(levelname)-8s[%(asctime)s] | %(message)s',
            'datefmt': '%d-%m %H:%M:%S'
        }
    },
    'handlers': {
        'file': {
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': 'log/out.log',
            'formatter': 'default',
            'level': 'INFO',
            'when': 'midnight',
            'interval': 1,
            'backupCount': 14,
        }
    },
    'root': {
        'level': 'INFO',
        'handlers': ['file']
    },
}
